# Secret role

## Description
Gets and sets secret variables.

## Requirements
Only the `debian` family of operating systems are supported.

## Role Variables

| Variable name          | Default                                                    | Description                        |
|------------------------|------------------------------------------------------------|------------------------------------|
| domain                 | Will try to get an ENV variable named `DOMAIN`             | Domain to deployment `Gitlab-CE`   |
| gitlab_url             | "https://gitlab.netology." + {{ domain }}                  | `Gitlab-CE` url                    |
| gitlab_registry_url    | "{{ gitlab_url }}:{{ gitlab_registry_port }}" | `Gitlab registry` url which a port |
| gitlab_registry_port   | Will try to get an ENV variable named `GITLAB_REGISTRY_PORT` | `Gitlab registry` port             |
| gitlab_registry_token  | Will try to get an ENV variable named `GITLAB_REGISTRY_TOKEN`  | `Gitlab registry` token            |
| base_dir  | The parent directory of the project  | Directory to save tmp files            |

## Example Playbook
```yaml
- hosts: all
  roles:
    - secret
```

## License
BSD

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
