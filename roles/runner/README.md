# Gitlab runner role

## Description
Installs `Gitlab runner`.

## Requirements
Only the `debian` family of operating systems are supported.

## Role Variables

| Variable name | Default                                             | Description                            |
|---------------|-----------------------------------------------------|----------------------------------------|
| gitlab_user  | "gitlab-runner"                                            | User to run `Gitlab-runner`            |
| domain  | Will try to get an ENV variable named `DOMAIN`      | Domain to deployment `Gitlab-CE`       |
| gitlab_url  | "https://gitlab.netology." + {{ domain }}           | `Gitlab-CE` url                        |
| base_dir  | The parent directory of the project  | Directory to save tmp files            |
| gitlab_registry_url  | Will try to get an ENV variable named `REGISTRATION_TOKEN`  | `Gitlab registry` url which a port     |

## Example Playbook
```yaml
- hosts: all
  roles:
    - runner
```

## License
BSD

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
