# Project role

## Description
Installs `Gitlab CE`.

## Requirements
Only the `debian` family of operating systems are supported.

## Role Variables

| Variable name | Default   | Description              |
|---------------|-----------|--------------------------|
| project_name  | "Must be defined" | Project name             |
| project_dir  | "Must be defined"  | Project directory        |
| project_repository_url  | "Must be defined"  | Project repository `URL` |


## Example Playbook
```yaml
- hosts: all
  roles:
    - project
```

## License
BSD

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
