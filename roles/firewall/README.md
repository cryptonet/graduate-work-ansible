# Firewall role

## Description
Configures the firewall.

## Requirements
Only the `debian` family of operating systems are supported.

## Role Variables

| Variable name | Default | Description             |
|---------------|--------|-------------------------|
| new_ssh_port  | 22     | New SSH connection port |

## Example Playbook
```yaml
- hosts: all
  roles:
    - firewall
```

## License
BSD

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
