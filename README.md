# The playbook to install `Gitlab-CE` and `Gitlab-runners`

## Description
This playbook is needed to install `Gitlab-CE`. The playbook exist the following roles:
* `gitlab` - installs and configures `Gitlab-CE`.
* `runner` - installs and configures `Gitlab-runner`.
* `project` - clones the test project into the created infrastructure.
* `firewall` - configures the firewall.
* `secret` - gets and sets secret variables and tokens.

## Installation steps  
***NOTE: Before launching, be sure to read the `README.md` in each role.***
The `dev` environment is used for testing the playbook on the local network.

### 1. Install base settings.
```shell
ansible-playbook -i inventory/prod/ -t base provision.yml
```

### 2. Install `Gitlab-CE`
```shell
ansible-playbook -i inventory/prod/ -t gitlab provision.yml
```
As a result of the script, information for connection will be displayed on the screen. 
You need to follow the link to the web `Gitlab-CE` interface and change the `root` password.

### 3. Secret variables and token
***NOTE: At this point, one action takes place - a registry token file is created to create a k8s cluster secret. In the future, item number 4 will be automated and moved here.***

#### 3.1. Create `Poject access token` token.
In the `Gitlab-CE` web interface, create [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) which name `webapp`.
After that you need to encode the token which the [manual](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#log-in-to-docker-hub).

```shell
ansible-playbook -i inventory/prod/ -t secret provision.yml
```

### 4. Prepare `Gitlab-CE` and get tokens.
This item has not been automated yet. All the following actions in this item will have to be performed manually.

#### 4.1 Create blank project.
You need to create a [blank project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-blank-project) which a same name as an ansible `project_name` variable exist.

#### 4.2 Get `url` repository.
Get `url` of new repository on a general page after create it.

#### 4.3 Add `ssh public key`.
Add your `ssh public key` to the project. And get a `fingerprint` after that.

#### 4.4. Set protect variables.
Set the following [protected variables](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) in the project web interface of `Gitlab-CE` instance.
* `K8S_SERVER` - Type -VARIABLE
* `K8S_CERTIFICATE_AUTHORITY_DATA` Type - VARIABLE
* `K8S_USER_TOKEN` Type - VARIABLE
* `GITLAB_REGISTRY_DATA` Type - FILE (has been created in third paragraph)

#### 4.5 Set variables.
In the `Gitlab-CE` web interface, set [protected tags](https://docs.gitlab.com/ee/user/project/protected_tags.html) according to the `v*` template.

#### 4.6. Get `registration token`.
Get `registration token` in the web `Gitlab-CE` interface, as says [here](https://docs.gitlab.com/runner/register/).
And don't forget disable shared runners.

If desired, you can set the `registration token` and `repository url` as an environment variable or enter it manually when starting the playbook in step 5.

For example:
```shell
export REGISTRATION_TOKEN=<REGISTRATION_TOKEN>
export GITLAB_REPOSITORY_URL=<GITLAB_REPOSITORY_URL>
```

#### 4.7. Make `A` or `AAAA` `DNS` record.### 3. Get registry auth data.
Make `A` or `AAAA` `DNS` record for `Gitlab-CE` server and wait until the host is available by domain name.
***NOTE: Without an `SSL` certificate, `gitlab registry` will not work, see the [problem](https://gitlab.com/gitlab-org/gitlab/-/issues/20810).***

### 5. Install `Gitlab-runner` and push the project.
```shell
ansible-playbook -i inventory/prod/ -t runner provision.yml
```

### 6. Clone project.
```shell
ansible-playbook -i inventory/prod/ -t project provision.yml
```

## Further development of the project
The project is far from ideal and will be constantly developing. The following tasks:
* Improve checking for the existence of a directory and installed packages also validity of certificates.
* Implement the change of the default `SSH` port.
* Issue and update SSL certificates.
* Change standard `TCP/UDP` ports for all external interfaces.
* Build docker image with pre-installed `qbec` to speed up the build process.
* Cover all roles with tests.

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
